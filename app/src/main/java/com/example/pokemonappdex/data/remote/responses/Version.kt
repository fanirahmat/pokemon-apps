package com.example.pokemonappdex.data.remote.responses

data class Version(
    val name: String,
    val url: String
)