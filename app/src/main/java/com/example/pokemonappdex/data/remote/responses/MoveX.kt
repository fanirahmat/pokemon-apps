package com.example.pokemonappdex.data.remote.responses

data class MoveX(
    val name: String,
    val url: String
)