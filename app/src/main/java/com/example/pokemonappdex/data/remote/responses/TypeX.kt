package com.example.pokemonappdex.data.remote.responses

data class TypeX(
    val name: String,
    val url: String
)