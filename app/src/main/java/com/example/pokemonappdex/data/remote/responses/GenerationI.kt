package com.example.pokemonappdex.data.remote.responses

data class GenerationI(
    val redBlue: RedBlue,
    val yellow: Yellow
)