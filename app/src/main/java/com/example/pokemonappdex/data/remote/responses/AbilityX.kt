package com.example.pokemonappdex.data.remote.responses

data class AbilityX(
    val name: String,
    val url: String
)