package com.example.pokemonappdex.data.remote.responses

data class Form(
    val name: String,
    val url: String
)