package com.example.pokemonappdex.data.remote.responses

data class GenerationIv(
    val diamondPearl: DiamondPearl,
    val heartGoldSoulSilver: HeartgoldSoulsilver,
    val platinum: Platinum
)