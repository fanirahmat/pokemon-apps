package com.example.pokemonappdex.data.remote.responses

data class GenerationVi(
    val omegaRubyAlphaSapphire: OmegarubyAlphasapphire,
    val xy: XY
)