package com.example.pokemonappdex.data.remote.responses

data class GenerationIi(
    val crystal: Crystal,
    val gold: Gold,
    val silver: Silver
)