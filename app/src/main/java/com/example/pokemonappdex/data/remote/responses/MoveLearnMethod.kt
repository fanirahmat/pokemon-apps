package com.example.pokemonappdex.data.remote.responses

data class MoveLearnMethod(
    val name: String,
    val url: String
)