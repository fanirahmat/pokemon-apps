package com.example.pokemonappdex.data.remote.responses

data class Result(
    val name: String,
    val url: String
)