package com.example.pokemonappdex.data.remote.responses

data class OfficialArtwork(
    val front_default: String
)