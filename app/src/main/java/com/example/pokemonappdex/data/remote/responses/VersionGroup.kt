package com.example.pokemonappdex.data.remote.responses

data class VersionGroup(
    val name: String,
    val url: String
)