package com.example.pokemonappdex.data.remote.responses

data class Type(
    val slot: Int,
    val type: TypeX
)