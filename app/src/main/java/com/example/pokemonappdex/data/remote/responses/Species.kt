package com.example.pokemonappdex.data.remote.responses

data class Species(
    val name: String,
    val url: String
)