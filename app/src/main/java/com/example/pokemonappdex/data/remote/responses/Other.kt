package com.example.pokemonappdex.data.remote.responses

data class Other(
    val dream_world: DreamWorld,
    val officialArtwork: OfficialArtwork
)