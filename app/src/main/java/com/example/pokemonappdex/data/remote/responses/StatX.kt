package com.example.pokemonappdex.data.remote.responses

data class StatX(
    val name: String,
    val url: String
)